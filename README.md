# xmonad-klt

My Xmonad configuration.
Try it out and improve it!

## Installation
Clone this repository into `${HOME}/.xmonad` and copy file `.xmobarrc` into your home folder, i.e.
``` shell
git clone git@codeberg.org:klt/xmonad-klt.git ${HOME}/.xmonad
cp -v ${HOME}/.xmonad/.xmobarrc ${HOME}
```
Two scripts need to be executed as super user. For this end add the following lines to `/etc/sudoers` and replace YOURUSERNAME by your user name:
```
YOURUSERNAME ALL=NOPASSWD: /home/YOURUSERNAME/.xmonad/multiplyBrightness.zsh
YOURUSERNAME ALL=NOPASSWD: /home/YOURUSERNAME/.xmonad/toggleBluetooth.zsh
```

## Prerequisites
This configuration has been tested on a Tuxedo laptop with Tuxedo OS which is based on Ubuntu 20.04 with budgie Desktop and its tools. You may need to adapt some settings according to your hardware and Linux version.

## Documentation
coming soon
