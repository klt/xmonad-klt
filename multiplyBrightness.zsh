#!/usr/bin/env zsh
WORKDIR=${0:h}
LOGFILE="${0:r}.log"
exec 1> ${LOGFILE} 2> ${LOGFILE}
SERVICES=($*)
source ${WORKDIR}/definition.zsh
logg "START $0 $*"

zmodload zsh/mathfunc
source /usr/share/zsh/functions/Math/zmathfunc

# You may need to set BRIGHTNESSDIR depending on your hardware
BRIGHTNESSDIR="/sys/devices/pci0000:00/0000:00:02.0/drm/card0/card0-eDP-1/intel_backlight/"
logg "BRIGHTNESSDIR = ${BRIGHTNESSDIR}"

factor=$1
currentBrightness=$(cat ${BRIGHTNESSDIR}/brightness)
maxBrightness=$(cat ${BRIGHTNESSDIR}/max_brightness)

logg "currentBrightness = $((currentBrightness))"
logg "maxBrightness = $((maxBrightness))"

# x is calculated new brightness
calculatedNewBrightness=$((int(rint(currentBrightness*factor))))
logg "calculatedNewBrightness = $((calculatedNewBrightness))"

# due to casting to integer x might not be different to currentBrightness
if (( calculatedNewBrightness == currentBrightness )); then
    if (( factor < 1.0 )); then
        y=$((currentBrightness-1))
    else
        y=$((currentBrightness+1))
    fi
else
    y=calculatedNewBrightness
fi

newBrightness=$((min(maxBrightness,max(0,y))))
logg "newBrightness = $((newBrightness))"
echo $((newBrightness)) > ${BRIGHTNESSDIR}/brightness

logg "DONE $0 $*"
