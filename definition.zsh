#!/usr/bin/env zsh
set -uo pipefail
# option -e cannot be set due to bug in zsh/mathfunc
# set -euo pipefail

function logg {
    echo $(date "+%H:%M:%S")" $1"
}

function isNumeric {
    local arg=$1
    local digitsOnly=$(echo "${arg}" | sed 's/[^[:digit:]]//g')
    if [[ ${arg} = ${digitsOnly} ]]; then
        echo "true"
    else
        echo "false"
    fi
}
