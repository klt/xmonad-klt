#!/usr/bin/env zsh
WORKDIR=${0:h}
LOGFILE="${0:r}.log"
SERVICES=($*)
source ${WORKDIR}/definition.zsh
set -e
logg "START $0 $*" > ${LOGFILE}

function setWifiStatus {
    local agendum=$1
    logg "nmcli radio wifi ${agendum}"
    nmcli radio wifi ${agendum}
    sleep 1
    logg "wifi status: $(nmcli radio wifi)"
}

function toggleWifiStatus {
    STATUS=$(nmcli radio wifi)
    logg "wifi status: ${STATUS}"
    if [[ $STATUS == "disabled" ]]; then
        setWifiStatus on
    else
        setWifiStatus off
    fi
}

toggleWifiStatus 1>> ${LOGFILE} 2>> ${LOGFILE}

logg "DONE $0 $*" >> ${LOGFILE}

xmessage -file ${LOGFILE}

exit
