#!/usr/bin/env zsh
SERVICES=("bluetooth" "postgresql" "wifi")

function showService {
    local service=$1
    if [[ ${service} == wifi ]]; then
        wifistatus=$(nmcli radio wifi)
        if [[ ${wifistatus} == enabled ]]; then
            echo "${service}"
        fi
    else
        systemctl --quiet is-active ${service}
        if (( $? == 0 )); then
            echo "${service}"
        fi
    fi
}

ACTIVESERVICES=( $(for svc in ${SERVICES}; do showService ${svc}; done) )

echo ${ACTIVESERVICES}

exit
