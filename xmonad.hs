import XMonad
import XMonad.Actions.SpawnOn
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.SetWMName
import XMonad.Layout.NoBorders (noBorders, smartBorders)
import XMonad.Layout.TwoPanePersistent
-- import XMonad.Layout.TwoPane (TwoPane(..))
import XMonad.Util.EZConfig

myBlue :: String
myBlue   = "#0080FF"
myRed :: String
myRed    = "#FF4A36"


-- Define how xmonad-workspace-status is presented in what color.
myXmobarPP :: PP
myXmobarPP = xmobarPP
  { ppCurrent = xmobarColor myBlue "" . wrap "[" "]"  -- currently focused workspace
  , ppTitle   = xmobarColor myRed ""   -- title of currently focused program
  -- ...
  }

-- Function to create the keyboard shortcut to show and hide the bar.
xmobarShowHideKeymap :: XConfig l -> (KeyMask, KeySym)
xmobarShowHideKeymap XConfig {modMask = modKey} = (modKey, xK_b)

myWorkspaces :: [WorkspaceId]
myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

myLayout = smartBorders $ tiled
  ||| Mirror tiled
--  ||| TwoPane delta ratio
  ||| TwoPanePersistent Nothing (3/100) (1/2)
  ||| noBorders Full
  where
    -- default tiling algorithm partitions the screen into two panes
    tiled   = Tall nmaster delta ratio
    -- The default number of windows in the master pane
    nmaster = 1
    -- Default proportion of screen occupied by master pane
    ratio   = 1/2
    -- Percent of screen to increment by when resizing panes
    delta   = 3/100

-- my apps
openProperties = spawnOn "9" "gnome-control-center 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startBrowser = spawnOn "2" "firefox 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startCalculator = spawn "qalculate-gtk 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startDbeaver = spawnOn "8" "flatpak run io.dbeaver.DBeaverCommunity  > ${HOME}/.xmonad/dbeaver.log"
startEmacs = spawnOn "1" "emacs 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startFilemanager = spawn "dolphin 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startRecording = spawnOn "5" "audacity 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startRawTerapee = spawnOn "7" "${HOME}/programs/rawtherapee/rawtherapee 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startScanner = spawn "simple-scan 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"
startTuxedoControlCenter = spawnOn "9" "tuxedo-control-center 1>> ${HOME}/.xmonad/xmonad.log 2>> ${HOME}/.xmonad/xmonad.log"

-- my services
audioLowerVolume = spawn "pactl set-sink-volume @DEFAULT_SINK@ -1%"
audioMute = spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle"
audioRaiseVolume = spawn "pactl set-sink-volume @DEFAULT_SINK@ +1%"
brightnessDown = spawn "brightnessctl set 1%- > ${HOME}/.xmonad/brightnessctl.log"
brightnessUp = spawn "brightnessctl set +1% > ${HOME}/.xmonad/brightnessctl.log"
hibernate = spawn "sudo ${HOME}/.xmonad/hibernate.zsh &"
screenSaver = spawn "xscreensaver-command -lock"
screenSetupRightLeft = spawn "${HOME}/.xmonad/setUpScreens.zsh true"
screenSetupLeftRight = spawn "${HOME}/.xmonad/setUpScreens.zsh false"
screenShot = spawn "scrot ${HOME}/Downloads/screen_%Y-%m-%d-%H%M%S.png -d 1"
toggleBluetooth = spawn "sudo ${HOME}/.xmonad/toggleService.zsh bluetooth"
togglePostgres = spawn "sudo ${HOME}/.xmonad/toggleService.zsh postgresql"
toggleWifi = spawn "${HOME}/.xmonad/toggleWifi.zsh"
windowShot = spawn "scrot ${HOME}/Downloads/window_%Y-%m-%d-%H%M%S.png -d 1 -u"

myStartupHook = do
  setWMName "LG3D"
  spawn "xsetroot -solid black"
  spawn "xscreensaver -verbose -log ${HOME}/.xmonad/xscreensaver.log"
--  spawnOn "9" "telegram-desktop"
  spawnOn "9" "flatpak run org.telegram.desktop > ${HOME}/.xmonad/telegram.log"
  spawnOn "9" "flatpak run --branch=stable --arch=x86_64 --command=nextcloud com.nextcloud.desktopclient.nextcloud > ${HOME}/.xmonad/nextcloud.log"


myConfig = def
  { modMask  = mod4Mask -- set 'Mod' to windows key
  , layoutHook = myLayout
  , terminal = "konsole"
  , borderWidth = 1
  , logHook  = dynamicLogString myXmobarPP >>= xmonadPropLog
  , manageHook = manageSpawn <+> manageHook def
  , workspaces = myWorkspaces
  , startupHook = myStartupHook
  } `additionalKeysP`
     [ ("M-x b", startDbeaver)
     , ("M-x d", startFilemanager)
     , ("M-x e", startEmacs)
     , ("M-x f", startBrowser)
     , ("M-x p", openProperties)
     , ("M-x r", startRecording)
     , ("M-x s", startScanner)
     , ("M-x t", startRawTerapee)
     , ("<Pause>", hibernate)
     , ("<XF86AudioMute>", audioMute)
     , ("<XF86AudioLowerVolume>", audioLowerVolume)
     , ("<XF86AudioRaiseVolume>", audioRaiseVolume)
     , ("<XF86Calculator>", startCalculator)
     , ("<XF86MonBrightnessUp>", brightnessUp)
     , ("<XF86MonBrightnessDown>", brightnessDown)
     , ("<Print>", screenShot)
     , ("M-<Print>", windowShot)
     , ("M-C-b", toggleBluetooth)
     , ("M-C-p", togglePostgres)
     , ("M-C-s", screenSetupRightLeft)
     , ("M-S-s", screenSetupLeftRight)
     , ("M-C-w", toggleWifi)
     , ("M-S-l", screenSaver)
     ]

main = xmonad =<< statusBar "xmobar" myXmobarPP xmobarShowHideKeymap myConfig
