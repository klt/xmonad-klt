#!/usr/bin/env zsh
MON=$(date "+%Y-%m")
LOGFILE="${0:r}_${MON}.log"
exec  1>>${LOGFILE} 2>>${LOGFILE}
WORKDIR=${0:h}
source ${WORKDIR}/definition.zsh
logg "START $0 $*"

# You may need to set STATEFILE depending on your distribution
STATEFILE="/sys/power/state"
logg "STATEFILE = ${STATEFILE}"
sleep 3
echo "disk" > ${STATEFILE}
logg "DONE $0 $*"
echo

exit $?
