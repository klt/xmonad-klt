#!/usr/bin/env zsh
WORKDIR=${0:h}
LOGFILE="${0:r}.log"
source ${WORKDIR}/definition.zsh
set -e
logg "START $0 $*" > ${LOGFILE}

FROM_RIGHT_TO_LEFT=$1

XRESDEFAULT=1920
YRESDEFAULT=1080

function setDefaultIfNonnumeric {
    local def=$1
    local arg=$2
    if [[ $(isNumeric ${arg}) == "true"  ]]; then
        echo "${arg}"
    else
        echo "${def}"
    fi
}

function setScreen {
    local isPirmary=$1
    local xpos=$2
    local screenNr=$3
    if [[ ${isPirmary} == "true" ]]; then
        primaryFlag="--primary"
    else
        primaryFlag=""
    fi
    local xres=$( setDefaultIfNonnumeric $((XRESDEFAULT)) ${XRES[screenNr]} )
    local yres=$( setDefaultIfNonnumeric $((YRESDEFAULT)) ${YRES[screenNr]} )
    logg "xrandr --output ${SCREENS[screenNr]} --pos $((xpos))x0 --mode $((xres))x$((yres)) ${primaryFlag}" >> ${LOGFILE}
    xrandr --output ${SCREENS[screenNr]} --pos $((xpos))x0 --mode $((xres))x$((yres)) ${primaryFlag}
    echo "$((xpos+xres))"
}

xrandr --auto
sleep 3

SCREENS=($(xrandr | grep " connected " | cut -d" " -f1))
NUM_SCREENS=$#SCREENS
logg "NUM_SCREENS = $((NUM_SCREENS))" >> ${LOGFILE}
XRES=($(xrandr | grep " connected " | sed 's/.*connected//;s/^[a-z,A-Z, ]*//;s/x.*//' ))
YRES=($(xrandr | grep " connected " | sed 's/.*connected//;s/^[a-z,A-Z, ]*//;s/+.*//;s/.*x//' ))

logg "FROM_RIGHT_TO_LEFT = ${FROM_RIGHT_TO_LEFT}" >> ${LOGFILE}
XPOS=0

if [[ ${FROM_RIGHT_TO_LEFT} == "true" ]]; then
    logg "Screen 1 will be on the right, screen $((NUM_SCREENS)) on the left, i.e. at x-position $((XPOS))" >> ${LOGFILE}
    XPOS=$( setScreen true $((XPOS)) $((NUM_SCREENS)) )
    for ((i=NUM_SCREENS-1;0<i;i--)) ; do
        XPOS=$( setScreen false $((XPOS)) $((i)) )
    done
else
    logg "Screen $((NUM_SCREENS)) will be on the right, screen 1 on the left, i.e. at x-position $((XPOS))" >> ${LOGFILE}
    XPOS=$( setScreen true $((XPOS)) 1 )
    for ((i=2;i<=NUM_SCREENS;i++)) ; do
        XPOS=$( setScreen false $((XPOS)) $((i)) )
    done
fi



logg "DONE $0 $*" >> ${LOGFILE}

xmessage -file ${LOGFILE}

exit
